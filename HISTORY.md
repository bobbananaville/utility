UTILITY
#Version 0.01
    Created the BitBucket Page
    
#Version 0.02
    Created the prototype. Now reads 3 files (SENTENCE, VARIABLES and REPLACER), replaces all VARIABLES in a random SENTENCE with a value from REPLACER associated with the VARIABLE.
    Created a function that reads a file that contains the files to read in the former function.
    
#Version 0.03
    Added a function that puts all replaced words into values in a dictionary, and the variables into keys.
    Changed the prototype function to utilize Dictionary instead of List
    Edited the replacer reading function so that the replacements don't correspond to the line number of the corresponding variable.
    
#Version 0.04
    Added a sentence function that divides sentences by '###' instead of by line.
    Created and uploaded the example set of txt files for presentation purposes.
    Added the ability to make multiple results at one time.
    Print all results to result.txt.
    
#Version 0.05
    Added two 'Run' files, one default and one custom. 
    Users can now change the 'result'.