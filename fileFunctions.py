import random

def helper(fileName):
    with open(fileName) as file:
        lineList=[]
        x=True
        while x==True:
            addToList=file.readline()
            if(addToList)=='':
                x=False
            addToList=addToList.replace("\n", "")
            lineList.append(addToList)
        del lineList[-1]
        return lineList

def printer(fileName, lineList):
    with open(fileName, 'w') as file:
        lineListLength=len(lineList)
        i=0
        while i<lineListLength:
            file.write(lineList[i]+"\n")
            i+=1

def wordSplitter(wordString):
    return wordString.split(";")

def listWords(lineList, variableList):
    returnDict={}
    currentVariableList=[]
    for line in lineList:
        if(line=="###"):
            currentVariableList=[]
        elif(line!=''):
            if(line in variableList and line not in currentVariableList):
                currentVariableList.append(line)
                if line not in returnDict:
                    returnDict[line]=[]
            elif(line not in currentVariableList):
                for currentVariable in currentVariableList:
                    returnDict[currentVariable].extend(wordSplitter(line))
    return returnDict

def listSentences(lineList):
    currentString=""
    stringList=[]
    for line in lineList:
        if(line=="###"):
            stringList.append(currentString)
            currentString=""
        else:
            currentString+=line
            currentString+="\n"
    return stringList
    
    
def lineChooser(lineList):
    return random.choice(lineList)

def emptyValueTest(lineList):
    for line in lineList:
        print("["+line+"]")

"""def main():
    fileName= input("Please input the file name: ")
    lineList=helper(fileName)
    for line in lineList:
        print (line)"""