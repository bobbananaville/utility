import random
import fileFunctions

"""def exampleFunction(sentence, listOfWords, listOfLists, times=3):
    wordsToReplace=len(listOfWords)
    i=0
    while(i<wordsToReplace):
        sentence=str.replace(sentence, listOfWords[i], random.choice(listOfLists[i]))
        i+=1
    if(times==0):
        return sentence
    else:
        times-=1
        return exampleFunction(sentence, listOfWords, listOfLists, times)"""
        
def exampleFunction(sentence, listOfWords, dictOfLists, times=3):
    for word in listOfWords:
        if(word!=''):
            sentence=str.replace(sentence, word, random.choice(dictOfLists.get(word)))
    if(times==0):
        return sentence
    else:
        times-=1
        return exampleFunction(sentence, listOfWords, dictOfLists, times)

# def fileTest():
#     print("REMEMBER:\n"+
#           "In the SENTENCE file, please input each possible sentence in its own line.\n"+
#           "In the VARIABLES file, please input each variable in its own line.\n"+
#           "In the REPLACER file, please input a list of words in each line, separated by #. Each line's list of words should correspond to the line in the VARIABLES file.")
    
#     sentence=fileFunctions.lineChooser(fileFunctions.helper(input("Please input the SENTENCE file name (IE: The sentence that you want to replace)")))
#     listOfWords=fileFunctions.helper(input("Please input the VARIABLES file name (IE: The variables in the sentence that you want to replace"))
#     listOfLists=fileFunctions.listWords(fileFunctions.helper(input("Please input the REPLACER file name (IE: The words that you want to replace the variables with")))
#     print(exampleFunction(sentence, listOfWords, listOfLists))

# def fileTestDefault():
#     sentence=fileFunctions.lineChooser(fileFunctions.helper("SENTENCE.txt"))
#     listOfWords=fileFunctions.helper("VARIABLES.txt")
#     listOfLists=fileFunctions.listWords(fileFunctions.helper("REPLACER.txt"))
    # print(exampleFunction(sentence, listOfWords, listOfLists))

def fileTestWithFileList(test=False, resultCheck=False):
    if(test==False):
        fileNameArray=fileFunctions.helper(input("Please input the LIST file name (IE: The list of files that will be used)\n"))
    else:
        fileNameArray=fileFunctions.helper("character.txt")
    sentence=fileFunctions.lineChooser(fileFunctions.listSentences(fileFunctions.helper(fileNameArray[0])))
    listOfWords=fileFunctions.helper(fileNameArray[1])
    dictOfLists=fileFunctions.listWords(fileFunctions.helper(fileNameArray[2]), listOfWords)
    #print(exampleFunction(sentence, listOfWords, listOfLists, 0))
    iterator=int(input("Please input a number greater than or equal to 1\n"))
    lineList=[]
    while(iterator>0):
        lineList.append(exampleFunction(sentence, listOfWords, dictOfLists))
        iterator-=1
    if(resultCheck==True):
        result=input("Please input the Result file name (IE: The file on which your results are printed)\nIt is highly recommended that you use a .txt file.\n")
    else:
        result="result.txt"
    fileFunctions.printer(result, lineList)
    
def dictTest():
    fileNameArray=fileFunctions.helper("character.txt")
    sentence=fileFunctions.lineChooser(fileFunctions.helper(fileNameArray[0]))
    listOfWords=fileFunctions.helper(fileNameArray[1])
    dictOfLists=fileFunctions.listWords(fileFunctions.helper(fileNameArray[2]), listOfWords)
    for word in listOfWords:
        if(word!=''):
            for item in dictOfLists[word]:
                print (word+" : "+item)


#fileTestWithFileList(True, True)
#dictTest()